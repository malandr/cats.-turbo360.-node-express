// Full Documentation - https://docs.turbo360.co
const vertex = require('vertex360')({site_id: process.env.TURBO_APP_ID})

const app = vertex.express() // initialize app

const index = require('./routes/index')
const api = require('./routes/api')
const cats = require('./routes/cats')

// set routes
app.use('/', index)
app.use('/api', api) // sample API Routes
app.use('/cats', cats)

module.exports = app