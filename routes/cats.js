// Full Documentation - https://docs.turbo360.co
const turbo = require('turbo360')({site_id: process.env.TURBO_APP_ID})
const vertex = require('vertex360')({site_id: process.env.TURBO_APP_ID})
const router = vertex.router()

const cats = {
    michael : {
        name: 'Michael'
    },
    joseph: {
        name: 'Joseph'
    }
}

router.get('/cats', (req, res) => {
    const michael = cats.michael
    res.render('cats', cats)

    // res.json({
    //     confirmation: 'Success',
    //     data: cats
    // })
})

router.get('/create-cat', (req, res) => {
    res.render('create-cat', null)
})

router.post('/create-cat', (req, res) => {
    const body = req.body
    res.json({
        confirmation : 'Success!',
        data : body
    })
})

module.exports = router